package me.christopher.addition;

import java.util.Scanner;

public class Addition {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        double a = 0.0, b = 0.0, erg = 0.0;

        // 1. Programmhinweis
        System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen");

        // 4. Eingabe
        System.out.println("1. Zahl:");
        a = scanner.nextDouble();
        System.out.println("2. Zahl:");
        b = scanner.nextDouble();

        output(a, b);

    }

    public static void output(double a, double b) {
        // 2. Output
        System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f + %.2f%n", process(a, b), a, b);

    }

    public static double process(double a, double b) {
        return a + b;
    }

}
