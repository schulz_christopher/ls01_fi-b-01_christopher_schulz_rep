package me.christopher.aufgabe.drei;

import java.text.DecimalFormat;

public class Main {

    public static void main(String[] args) {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i <= 20; i++) {
            builder.append("-");
        }



        System.out.println(String.format("%s %2s %3s", "Fahrenheit", "|", "Celsius"));
        System.out.println(builder.toString());
        System.out.println(String.format("%s %9s %7.2f", "-20", "|", -28.8889));
        System.out.println(String.format("%s %9s %7s", "-10", "|", round(-23.3333)));
        System.out.println(String.format("%s %11s %7s", "0", "|", round(-17.7778)));
        System.out.println(String.format("%s %10s %7s", "20", "|", round(-6.6667)));
        System.out.println(String.format("%s %10s %7s", "30", "|", round(-1.1111)));

    }

    public static String round(double toRound) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        return decimalFormat.format(toRound);
    }


}
