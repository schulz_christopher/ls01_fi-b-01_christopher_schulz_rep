package me.christopher.aufgabe.zwei;

public class Main {

    public static void main(String[] args) {

        System.out.println(String.format("0! %3s %19s %4s", "=", "=", "1"));
        System.out.println(String.format("1! %3s 1 %17s %4s", "=", "=", "1"));
        System.out.println(String.format("2! %3s 1 * 2 %13s %4s", "=", "=", "2"));
        System.out.println(String.format("3! %3s 1 * 2 * 3 %9s %4s", "=", "=", "6"));
        System.out.println(String.format("4! %3s 1 * 2 * 3 * 4 %5s %4s", "=", "=", "24"));
        System.out.println(String.format("5! %3s 1 * 2 * 3 * 4 * 5 %s %4s", "=", "=", "120"));

    }

}
